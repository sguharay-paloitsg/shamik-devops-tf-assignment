**README**
**Introduction**

This repository contains code for executing Terraform scripts locally and testing AWS Lambda function and APIGateway deployed on my local environment using LocalStack.

**Prerequisites**
- Terraform installed on my local machine.
- LocalStack setup for emulating AWS services locally


**Test Cases:**

Below test cases were successfully executed. 

_Task A: "Create lambda function"_
- make list1 
- make test1 (Invoke lambda function)

_Task B: "Create API Gateway"_
- make list2
- make test2

_Task C: "Add API Key to API Gateway"_
- make test3

_Task D: "Use Custom Domain"_
- make test4

_Task E: "Create Gitlab CI/CD Pipeline"._

Refer Screenshot in attachment : "**TEST_PASSED_Screenshot.png**"



**Testing Pipeline:**

After executing all the above test cases in my local environment, a CI/CD pipeline has been set up in GitLab. This pipeline automates the process of testing and deploying changes to the production environment.

 
