output "lambda" {
  value = aws_lambda_function.this
}

# TASK B
output "apigateway" {
  value = aws_api_gateway_rest_api.this
}