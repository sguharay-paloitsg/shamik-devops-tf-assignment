resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.this.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.this.execution_arn}/*/*"
}

# TASK B

resource "aws_api_gateway_rest_api" "this" {
  name        = var.name
  description = "Test"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "root" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_rest_api.this.root_resource_id
  path_part   = ""
}

resource "aws_api_gateway_method" "this" {
  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = aws_api_gateway_resource.root.id
  http_method   = var.http_method
  authorization = var.authorization
}

resource "aws_api_gateway_integration" "this" {
  rest_api_id             = aws_api_gateway_rest_api.this.id
  resource_id             = aws_api_gateway_resource.root.id
  http_method             = aws_api_gateway_method.this.http_method
  integration_http_method = var.integration_http_method
  type                    = var.integration_type

  uri = aws_lambda_function.this.invoke_arn
}

resource "aws_api_gateway_deployment" "this" {
  depends_on      = [aws_api_gateway_integration.this]
  rest_api_id     = aws_api_gateway_rest_api.this.id
  stage_name      = var.stage
}


# TASK C

resource "aws_api_gateway_api_key" "this" {
  name = "helloworld-localstack-test"
}

resource "aws_api_gateway_usage_plan" "this" {
  name = "helloworld-localstack-usage-plan"

  api_stages {
    api_id  = aws_api_gateway_rest_api.this.id
    stage   = aws_api_gateway_deployment.this.stage_name
  }

  throttle_settings {
    burst_limit = 5000
    rate_limit  = 10000
  }

  quota_settings {
    limit = 10000
    offset = 10
    period = "MONTH"
  }
}

resource "aws_api_gateway_usage_plan_key" "this" {
  key_id        = aws_api_gateway_api_key.this.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.this.id
}


# TASK D

resource "aws_route53_zone" "this" {
  name = var.api_domain_name  # Change this to your actual domain name
}

resource "aws_api_gateway_domain_name" "this" {
  domain_name     = var.api_domain_name
}

resource "aws_api_gateway_base_path_mapping" "this" {
  domain_name = aws_api_gateway_domain_name.this.domain_name
  stage_name  = aws_api_gateway_deployment.this.stage_name
  api_id      = aws_api_gateway_rest_api.this.id
}

resource "aws_route53_record" "this" {
  depends_on = [aws_route53_zone.this]  # Ensure that the zone is created before creating the record
  zone_id    = aws_route53_zone.this.zone_id
  name       = aws_api_gateway_domain_name.this.domain_name
  type       = "A"

  alias {
    name                   = aws_api_gateway_domain_name.this.domain_name
    zone_id                = aws_route53_zone.this.zone_id
    evaluate_target_health = false
  }
}
